package aoc

import (
	"context"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var server *httptest.Server

func TestMain(m *testing.M) {
	server = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte(resp))
	}))
	code := m.Run()
	server.Close()
	os.Exit(code)
}

func TestAOC(t *testing.T) {
	client := New("", "", "", WithHTTP(server.Client()))
	client.endpoint = server.URL

	lb, err := client.Leaderboard(context.Background())
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	// Invalid member
	_, err = lb.Member("invalid")
	if err == nil || !IsErrMemberNotExist(err) {
		t.Log("invalid member")
		t.FailNow()
	}

	member, err := lb.Member("jolheiser")
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	// Invalid Day (not completed yet)
	_, err = member.DayLevel(Day25, Level1)
	if err == nil || !IsErrDayNotExist(err) {
		t.Log("incomplete day")
		t.FailNow()
	}

	// Invalid Day (DIY)
	_, err = member.DayLevel("26", Level1)
	if err == nil || !IsErrDayNotExist(err) {
		t.Log("invalid day")
		t.FailNow()
	}

	// Invalid Level (not completed yet)
	_, err = member.DayLevel(Day8, Level2)
	if err == nil || !IsErrLevelNotExist(err) {
		t.Log("incomplete level")
		t.FailNow()
	}

	// Invalid Level (DIY)
	_, err = member.DayLevel(Day8, "3")
	if err == nil || !IsErrLevelNotExist(err) {
		t.Log("invalid level")
		t.FailNow()
	}

	// Valid day and level
	_, err = member.DayLevel(Day8, Level1)
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	// Rank
	rank := lb.Rank()
	for idx, id := range []string{"983788", "1043766", "216626"} {
		if rank[idx].ID != id {
			t.Log("incorrect ranking")
			t.FailNow()
		}
	}
}

var resp = `{"members":{"216626":{"completion_day_level":{"1":{"1":{"get_star_ts":"1606838340"},"2":{"get_star_ts":"1606843057"}},"2":{"1":{"get_star_ts":"1606919969"},"2":{"get_star_ts":"1606920829"}},"3":{"1":{"get_star_ts":"1607016457"},"2":{"get_star_ts":"1607017454"}},"4":{"1":{"get_star_ts":"1607115604"},"2":{"get_star_ts":"1607121281"}},"5":{"1":{"get_star_ts":"1607184543"},"2":{"get_star_ts":"1607185489"}},"6":{"1":{"get_star_ts":"1607231498"},"2":{"get_star_ts":"1607232295"}},"7":{"1":{"get_star_ts":"1607363891"},"2":{"get_star_ts":"1607367719"}},"8":{"1":{"get_star_ts":"1607439245"},"2":{"get_star_ts":"1607441985"}}},"stars":16,"last_star_ts":"1607441985","id":"216626","name":"joeyahines","local_score":22,"global_score":0},"983788":{"stars":16,"completion_day_level":{"1":{"1":{"get_star_ts":"1606833744"},"2":{"get_star_ts":"1606833895"}},"2":{"1":{"get_star_ts":"1606925904"},"2":{"get_star_ts":"1606926534"}},"3":{"1":{"get_star_ts":"1606974549"},"2":{"get_star_ts":"1606975051"}},"4":{"1":{"get_star_ts":"1607060015"},"2":{"get_star_ts":"1607060767"}},"5":{"1":{"get_star_ts":"1607145711"},"2":{"get_star_ts":"1607147659"}},"6":{"1":{"get_star_ts":"1607269028"},"2":{"get_star_ts":"1607269212"}},"7":{"1":{"get_star_ts":"1607319690"},"2":{"get_star_ts":"1607319986"}},"8":{"1":{"get_star_ts":"1607404810"}}},"last_star_ts":"1607405703","id":"983788","name":"jolheiser","local_score":35,"global_score":0},"1043766":{"id":"1043766","name":"KevBelisle","completion_day_level":{"1":{"1":{"get_star_ts":"1606839553"},"2":{"get_star_ts":"1606839989"}},"2":{"1":{"get_star_ts":"1606919226"},"2":{"get_star_ts":"1606919432"}},"3":{"1":{"get_star_ts":"1607006831"},"2":{"get_star_ts":"1607008208"}},"4":{"1":{"get_star_ts":"1607110923"},"2":{"get_star_ts":"1607111571"}},"5":{"1":{"get_star_ts":"1607200070"},"2":{"get_star_ts":"1607200424"}},"6":{"1":{"get_star_ts":"1607281195"},"2":{"get_star_ts":"1607281881"}},"7":{"1":{"get_star_ts":"1607355003"},"2":{"get_star_ts":"1607365033"}},"8":{"1":{"get_star_ts":"1607404758"},"2":{"get_star_ts":"1607406416"}}},"last_star_ts":"1607406416","stars":16,"global_score":0,"local_score":27}},"event":"2020","owner_id":"983788"}`
