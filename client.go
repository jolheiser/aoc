package aoc

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

const interval = time.Minute * 15

// Client is an AoC client
type Client struct {
	http   *http.Client
	year   string
	code   string
	Cookie string

	endpoint string
	cache    *Leaderboard
	last     time.Time
}

// Year returns a Client's year
func (c *Client) Year() string {
	return c.year
}

// Code returns a Client's code
func (c *Client) Code() string {
	return c.code
}

// ShortCode returns the private leaderboard code
// Hint: It's the owner's ID
func (c *Client) ShortCode() string {
	return strings.Split(c.code, "-")[0]
}

// New creates a new Client
func New(year, code, cookie string, opts ...ClientOption) *Client {
	c := &Client{
		http:   http.DefaultClient,
		year:   year,
		code:   code,
		Cookie: cookie,
		// Fifteen minutes ago, allowing us to get "new" results immediately
		last: time.Now().Add(-interval),
	}

	for _, opt := range opts {
		opt(c)
	}
	c.endpoint = fmt.Sprintf("https://adventofcode.com/%s/leaderboard/private/view/%s.json", year, code)

	return c
}

// ClientOption is a configuration for a Client
type ClientOption func(*Client)

// WithHTTP configures the http.Client for the Client
func WithHTTP(client *http.Client) ClientOption {
	return func(c *Client) {
		c.http = client
	}
}

// Leaderboard returns the Leaderboard results
func (c *Client) Leaderboard(ctx context.Context) (*Leaderboard, error) {
	cutoff := time.Now().Add(-interval)
	if c.last.After(cutoff) {
		return c.cache, ErrNotUpdated{Last: c.last, Next: c.last.Sub(cutoff)}
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, c.endpoint, nil)
	if err != nil {
		return nil, err
	}
	req.AddCookie(&http.Cookie{
		Name:  "session",
		Value: c.Cookie,
	})

	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}

	// Invalid session
	if resp.StatusCode == http.StatusFound {
		return c.cache, errors.New("302 - usually indicates an invalid session")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var leaderboard Leaderboard
	if err := json.Unmarshal(body, &leaderboard); err != nil {
		return nil, err
	}

	c.cache = &leaderboard
	return &leaderboard, nil
}
