package aoc

import (
	"sort"
	"strconv"
	"strings"
	"time"
)

// Leaderboard is a private AoC leaderboard
type Leaderboard struct {
	Members map[string]*Member `json:"members"`
	Event   string             `json:"event"`
	OwnerID string             `json:"owner_id"`
}

// Member returns a member by their username
func (l *Leaderboard) Member(name string) (*Member, error) {
	for _, member := range l.Members {
		if strings.EqualFold(member.Name, name) {
			return member, nil
		}
	}
	return nil, ErrMemberNotExist{name: name}
}

// Rank returns a members from highest to lowest local score
func (l *Leaderboard) Rank() []*Member {
	members := make(memberList, len(l.Members))
	var idx int
	for _, member := range l.Members {
		members[idx] = member
		idx++
	}
	sort.Sort(members)
	return members
}

// Member is the member information for a Leaderboard
type Member struct {
	ID                 string                      `json:"id"`
	Name               string                      `json:"name"`
	Stars              int                         `json:"stars"`
	LastStarTS         string                      `json:"last_star_ts"`
	LocalScore         int                         `json:"local_score"`
	GlobalScore        int                         `json:"global_score"`
	CompletionDayLevel map[Day]map[Level]*DayLevel `json:"completion_day_level"`
}

// DayLevel gets the day and level information from a Member
func (m *Member) DayLevel(day Day, level Level) (*DayLevel, error) {
	d, ok := m.CompletionDayLevel[day]
	if !ok {
		return nil, ErrDayNotExist{Day: day, Member: m.Name}
	}

	l, ok := d[level]
	if !ok {
		return nil, ErrLevelNotExist{Day: day, Level: level, Member: m.Name}
	}

	return l, nil
}

// LastStart returns a time.Time representation for LastStarTS
func (m *Member) LastStar() (time.Time, error) {
	unix, err := strconv.ParseInt(m.LastStarTS, 10, 64)
	if err != nil {
		return time.Time{}, err
	}
	return time.Unix(unix, 0), nil
}

// DayLevel is the star information for a day and level
type DayLevel struct {
	GetStarTS string `json:"get_star_ts"`
}

// GetStar returns a time.Time representation of GetStarTS
func (d *DayLevel) GetStar() (time.Time, error) {
	unix, err := strconv.ParseInt(d.GetStarTS, 10, 64)
	if err != nil {
		return time.Time{}, err
	}
	return time.Unix(unix, 0), nil
}

// Day is an AoC day
type Day string

const (
	Day1  Day = "1"
	Day2  Day = "2"
	Day3  Day = "3"
	Day4  Day = "4"
	Day5  Day = "5"
	Day6  Day = "6"
	Day7  Day = "7"
	Day8  Day = "8"
	Day9  Day = "9"
	Day10 Day = "10"
	Day11 Day = "11"
	Day12 Day = "12"
	Day13 Day = "13"
	Day14 Day = "14"
	Day15 Day = "15"
	Day16 Day = "16"
	Day17 Day = "17"
	Day18 Day = "18"
	Day19 Day = "19"
	Day20 Day = "20"
	Day21 Day = "21"
	Day22 Day = "22"
	Day23 Day = "23"
	Day24 Day = "24"
	Day25 Day = "25"
)

// Level is an AoC level for a Day
type Level string

const (
	Level1 Level = "1"
	Level2 Level = "2"
)

// Member sorting
type memberList []*Member

func (e memberList) Len() int {
	return len(e)
}

func (e memberList) Less(i, j int) bool {
	return e[i].LocalScore > e[j].LocalScore
}

func (e memberList) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}
