package aoc

import (
	"fmt"
	"time"
)

// ErrNotUpdated is an error when a leaderboard is requested too soon
type ErrNotUpdated struct {
	Last time.Time
	Next time.Duration
}

// Error implements the error interface
func (e ErrNotUpdated) Error() string {
	return fmt.Sprintf("last try was at %s, try again in %s", e.Last, e.Next)
}

// IsErrNotUpdated checks whether an err is ErrNotUpdated
func IsErrNotUpdated(err error) bool {
	_, ok := err.(ErrNotUpdated)
	return ok
}

// ErrMemberNotExist is an error when a Member could not be found by name
type ErrMemberNotExist struct {
	name string
}

// Error implements the error interface
func (e ErrMemberNotExist) Error() string {
	return fmt.Sprintf("could not find member with name %s", e.name)
}

// IsErrMemberNotExist checks whether an err is ErrMemberNotExist
func IsErrMemberNotExist(err error) bool {
	_, ok := err.(ErrMemberNotExist)
	return ok
}

// ErrDayNotExist is an error when a day does not exist for a Member
type ErrDayNotExist struct {
	Member string
	Day    Day
}

// Error implements the error interface
func (e ErrDayNotExist) Error() string {
	return fmt.Sprintf("%s did not start day %s", e.Member, e.Day)
}

// IsErrDayNotExist checks whether an err is ErrDayNotExist
func IsErrDayNotExist(err error) bool {
	_, ok := err.(ErrDayNotExist)
	return ok
}

// ErrLevelNotExist is an error when a level does not exist for a Member on a Day
type ErrLevelNotExist struct {
	Member string
	Day    Day
	Level  Level
}

// Error implements the error interface
func (e ErrLevelNotExist) Error() string {
	return fmt.Sprintf("%s did not complete day %s level %s", e.Member, e.Day, e.Level)
}

// IsErrLevelNotExist checks whether an err is ErrLevelNotExist
func IsErrLevelNotExist(err error) bool {
	_, ok := err.(ErrLevelNotExist)
	return ok
}
